import terser from '@rollup/plugin-terser';
import { babel } from '@rollup/plugin-babel';
import copy from 'rollup-plugin-copy';

import pkg from './package.json';

const version = pkg.version ?? 'DEV';
const date = new Date().toJSON().split('T')[0];
const banner = `/*! ${pkg.name} - v${version} - ${date} */`;

export default [
  {
    input: 'src/a11y.autocomplete.js',
    output: {
      // @todo change to `autocomplete`
      name: 'A11yAutocomplete',
      file: pkg.browser,
      format: 'iife',
      sourcemap: true,
    },
    plugins: [
      // Minify resulting ES5 code, add back banner.
      copy({ targets: [{ src: 'src/a11y.autocomplete.css', dest: 'dist' }] }),
      babel({ babelHelpers: 'bundled' }),
      terser({ format: { comments: false, preamble: banner } }),
    ],
  },
];
