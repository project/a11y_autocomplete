import { sendKeys, executeServerCommand } from '@web/test-runner-commands';

/**
 *
 * @param {number} amount
 *  Number of milliseconds to wait.
 * @return {Promise}
 *  A Promise that resolves after the set amount of milliseconds.
 */
function delay(amount = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, amount);
  });
}

/**
 * Simulate typing into a text field triggering the right events.
 *
 * @param {HTMLInputElement|HTMLTextAreaElement} element
 *  An form element that can be typed into.
 * @param {string} text
 *  The text to type in the element.
 * @param {object} [options={}]
 *  Extra options to send to the test runner.
 * @return {Promise}
 *  A Promise that resolves after the `wait` amount of milliseconds.
 */
async function type(element, text, options = {}) {
  element.focus();
  if (options.clear) {
    await clear(element);
  }
  await sendKeys({ type: text });
  return delay(options.wait || 0);
}

/**
 * Simulate a key press.
 *
 * @param {string} key
 *  The key to press.
 * @param {object} [options={}]
 *  Extra options to send to the test runner.
 * @return {Promise}
 *  A Promise that resolves after the `wait` amount of milliseconds.
 */
async function press(key, options = {}) {
  await sendKeys({ press: key });
  return delay(options.wait || 0);
}

/**
 * Empties the element's value.
 *
 * @param {HTMLInputElement|HTMLTextAreaElement} element
 *  An form element that can be typed into.
 * @param {object} [options={}]
 *  Extra options to send to the test runner.
 * @return {Promise}
 *  A Promise that resolves after the `wait` amount of milliseconds.
 */
function clear(element, options = {}) {
  element.value = '';

  return delay(options.wait || 0);
}

/**
 * Helper to simulate a mouse click on a specific element.
 *
 * @param {Element} element
 *  An form element that can be typed into.
 * @param {object} [options={}]
 *  Extra options to send to the test runner to configure the mouse click.
 * @return {Promise}
 *  A Promise that resolves after the `wait` amount of milliseconds.
 */
async function click(element, options = {}) {
  const x = element.offsetLeft + element.offsetWidth / 2;
  const y = element.offsetTop + element.offsetHeight / 2;

  await executeServerCommand('mouse-click', { x, y });
  return delay(options.wait || 0);
}

export default { type, clear, press, click };
