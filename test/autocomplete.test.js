/**
 * @file
 * Main library tests. Requires a browser to run.
 */
import { expect, fixture } from '@open-wc/testing';
import A11yAutocomplete from '../src/a11y.autocomplete.js';
import device from './device';

describe('autocomplete', () => {
  // Default wait time for the listbox to show up.
  const wait = 350;
  const source = (query, result) => {
    result(list.filter((item) => item.includes(query)));
  };
  const data = [
    { value: 1, label: 'one', group: 'odd' },
    { value: 2, label: 'two', group: 'even' },
    { value: 3, label: 'three', group: 'odd' },
    { value: 4, label: 'four', group: 'even' },
    { value: 5, label: 'five', group: 'odd' },
    { value: 6, label: 'six', group: 'even' },
    { value: 7, label: 'seven', group: 'odd' },
    { value: 8, label: 'eight', group: 'even' },
    { value: 9, label: 'nine', group: 'odd' },
    { value: 10, label: 'ten', group: 'even' },
    { value: 'special_value', label: 'special,value' },
  ];
  const list = data.map(({ label }) => label);

  it('create autocomplete', async () => {
    const input = await fixture(`<input>`);
    const promise = new Promise((resolve) => {
      document.addEventListener('autocomplete-created', ({ detail }) => {
        expect(detail.autocomplete).to.be.an('object');
        expect(detail.autocomplete.id).to.be.a('string');
        expect(detail.autocomplete.destroy).to.be.a('function');
        resolve();
      });
    });
    A11yAutocomplete(input);
    return promise;
  });

  it('Make sure the wrapper replaces the input at the same place in the DOM Tree', async () => {
    const wrapper = await fixture(
      `<div><span></span><input><span></span></div>`,
    );
    // The second item is an input that will be replaced by a div node.
    const expectedTagList = ['span', 'div', 'span'];
    const input = wrapper.querySelector('input');
    A11yAutocomplete(input);
    const resultingTagList = Array.from(wrapper.children).map(
      (el) => el.localName,
    );
    expect(resultingTagList).to.be.eql(expectedTagList);
  });

  it('use options from the constructor', async () => {
    const input = await fixture(`<input>`);
    const auto = A11yAutocomplete(input, { source: [], minChars: 2 });
    const options = auto._internal_object.options;

    expect(options).to.have.deep.property('source', []);
    expect(options).to.have.property('minChars', 2);
  });

  it('parse options from data-autocomplete attribute', async () => {
    const input = await fixture(
      `<input data-autocomplete='{"source":[], "minChars":2}'>`,
    );
    const auto = A11yAutocomplete(input);
    const options = auto._internal_object.options;

    expect(options).to.have.deep.property('source', []);
    expect(options).to.have.property('minChars', 2);
  });

  it('parse options from data-autocomplete-* attributes', async () => {
    const input = await fixture(
      `<input data-autocomplete-source="[]" data-autocomplete-min-chars="2">`,
    );
    const auto = A11yAutocomplete(input);
    const options = auto._internal_object.options;

    expect(options).to.have.deep.property('source', []);
    expect(options).to.have.property('minChars', 2);
  });

  it('verify options precedence', async () => {
    const input = await fixture(`<input
      data-autocomplete='{"source":[], "minChars":2}'
      data-autocomplete-source="['ignore-data-attribute']" data-autocomplete-min-chars="4"
    >`);
    const auto = A11yAutocomplete(input, {
      source: ['ignore-constructor'],
      minChars: 10,
    });
    const options = auto._internal_object.options;

    expect(options).to.have.deep.property('source', []);
    expect(options).to.have.property('minChars', 2);
  });

  it('aria-describedby creation', async () => {
    const id = 'autocomplete';
    const assistiveId = `${id}-assistive-hint`;
    const input = await fixture(`<input id="${id}">`);
    A11yAutocomplete(input);
    const wrap = expect(input.parentNode).dom;
    expect(input).to.have.attribute('aria-describedby', assistiveId);
    wrap.to.have.descendant(`#${assistiveId}`);

    await device.type(input, 't', { wait });
    expect(input).to.not.have.attribute('aria-describedby');
  });

  it('aria-describedby reuse', async () => {
    const id = 'autocomplete';
    const assistiveId = `${id}-assistive-hint`;
    const input = await fixture(
      `<input id="${id}" aria-describedby="existing-id">`,
    );
    A11yAutocomplete(input);
    const wrap = expect(input.parentNode).dom;
    expect(input).to.have.attribute(
      'aria-describedby',
      `existing-id ${assistiveId}`,
    );
    wrap.to.have.descendant(`#${assistiveId}`);

    await device.type(input, 't', { wait });
    expect(input).to.have.attribute('aria-describedby', 'existing-id');
  });

  it('option filtering', async () => {
    // Make sure only supported options can be defined using any method.
  });

  it('option type casting', async () => {
    // Test transform of true/false string into bool
    // Test casting of cardinality/minChars to int
    // Test transform of list as string to list as array
  });

  it('option source as list', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source: list });
    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait });
    wrap.have.descendants('[role="option"]').and.have.length(4);

    await device.type(input, 'w', { wait });
    wrap.have.descendants('[role="option"]').and.have.length(1);
  });

  it('option source as string', async () => {
    const input = await fixture(
      `<input data-autocomplete-source='${JSON.stringify(list)}'>`,
    );
    A11yAutocomplete(input);
    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait });
    wrap.have.descendants('[role="option"]').and.have.length(4);

    await device.type(input, 'w', { wait });
    wrap.have.descendants('[role="option"]').and.have.length(1);
  });

  it('option source as non-expected type', async () => {
    const input = await fixture(`<input>`);
    expect(() => A11yAutocomplete(input, { source: {} })).to.throw(TypeError);
  });

  it('option source as function', async () => {
    const input = await fixture(`<input>`);
    const search = 'tw';
    const sourceMock = (query, result) => {
      expect(query).equal(search);
      source(query, result);
    };
    A11yAutocomplete(input, { source: sourceMock });
    const wrap = expect(input.parentNode).dom;

    // Makes sure results are displayed.
    await device.type(input, search, { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
  });

  it('no filtering for results when option source as function', async () => {
    const input = await fixture(`<input>`);
    const search = 'some extremely specific kittens';
    const sourceMock = (query, result) => {
      expect(query).equal(search);
      result(data);
    };
    A11yAutocomplete(input, { source: sourceMock });
    const wrap = expect(input.parentNode).dom;

    // Makes sure results are not filtered.
    await device.type(input, search, { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(11);
  });

  [
    { type: 'static', source: list },
    { type: 'callback', source },
  ].forEach(({ type, source }) => {
    it(`${type}: option minChars`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, { source, minChars: 2 });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 't', { wait });
      wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
      wrap.have.not.descendants('[role="option"]');

      await device.type(input, 'w', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(1);

      // Make sure the suggestion list closes when going below minChars
      await device.press('Backspace', { wait });
      expect(input.value).to.be.equal('t');
      wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
      wrap.have.not.descendants('[role="option"]');
    });

    it(`${type}: option cardinality: one`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, { source, cardinality: 1 });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 't', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(4);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two');

      // Don't suggest things after the separator char.
      await device.type(input, ', f', { wait });
      wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
      wrap.have.not.descendants('[role="option"]');
    });

    it(`${type}: option cardinality: more than one`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, { source, cardinality: 2 });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'two, f', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(2);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two, four');

      await device.type(input, ', o', { wait });
      wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
      wrap.have.not.descendants('[role="option"]');
    });

    it(`${type}: option cardinality: unlimited`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, { source, cardinality: -1 });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'two, f', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(2);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two, four');

      await device.type(input, ', o', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]');
    });

    it(`${type}: option separatorChar`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source,
        separatorChar: ';',
        cardinality: -1,
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'two; f', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(2);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two; four');
    });

    it(`${type}: option firstCharacterIgnoreList`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source,
        firstCharacterIgnoreList: '@',
        cardinality: 2,
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, '@t', { wait });
      wrap.have.descendant('[role="listbox"]').not.to.be.displayed;
      wrap.have.not.descendants('[role="option"]');

      await device.type(input, 'wo, t', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(4);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('@two, two');
    });

    // The suggestion list should close if the first char becomes a ignore char.
    it(`${type}: option firstCharacterIgnoreList change input first char`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source,
        firstCharacterIgnoreList: '@',
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 't', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(4);
      await device.press('ArrowLeft');
      await device.type(input, '@', { wait });
      wrap.have.descendant('[role="listbox"]').not.to.be.displayed;
      wrap.have.not.descendants('[role="option"]');
      expect(input.value).to.be.equal('@t');
    });

    it(`${type}: option templates suggestion`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source: data,
        templates: {
          suggestion: (suggestion) =>
            `${suggestion.label} (${suggestion.value})`,
        },
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 't', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendant('[role="option"]').to.have.text('two (2)');
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('2');
    });

    it(`${type}: option templates inputValue`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source: data,
        templates: {
          inputValue: (suggestion) => `inputValue: ${suggestion.value}`,
        },
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 't', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendant('[role="option"]').to.have.text('two');
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('inputValue: 2');
    });

    it(`${type}: option allowRepeatValues: default`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source: list,
        cardinality: 2,
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'two, t', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(3);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two, three');
    });

    it(`${type}: option allowRepeatValues: true`, async () => {
      const input = await fixture(`<input>`);
      A11yAutocomplete(input, {
        source: list,
        allowRepeatValues: true,
        cardinality: 2,
      });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'two, t', { wait });
      wrap.have.descendant('[role="listbox"]').to.be.displayed;
      wrap.have.descendants('[role="option"]').and.have.length(4);
      await device.press('ArrowDown');
      await device.press('Enter');
      expect(input.value).to.be.equal('two, two');
    });

    it(`${type}: alter results with aucomplete-response event`, async () => {
      const input = await fixture(`<input>`);
      let alteredResponseLength = 0;
      input.addEventListener(`autocomplete-response`, ({ detail }) => {
        // Remove the first result from the list.
        detail.list.splice(0, 1);
        alteredResponseLength = detail.list.length;
      });
      A11yAutocomplete(input, { source });
      const wrap = expect(input.parentNode).dom;

      await device.type(input, 'o', { wait, clear: true });
      wrap.have.descendants('[role="option"]').length(alteredResponseLength);
    });
  });

  it('option createLiveRegion', async () => {});

  it('option autoFocus', async () => {});

  it('api .destroy()', async () => {
    const originalInput = `<input data-autocomplete="">`;
    const wrapper = await fixture(`<div>${originalInput}</div>`);
    const input = wrapper.firstChild;
    const auto = A11yAutocomplete(input);
    expect(input).to.have.attribute('aria-expanded', 'false');
    expect(input).to.have.attribute('aria-owns');

    auto.destroy();
    expect(wrapper).dom.to.have.html(originalInput);
  });

  it('api .id', async () => {
    // Auto generate an id.
    const autoNoId = A11yAutocomplete(await fixture(`<input>`));
    expect(autoNoId.id).to.be.equal('autocomplete-input-0');

    // Use existing id
    const autoWithId = A11yAutocomplete(await fixture(`<input id="test-id">`));
    expect(autoWithId.id).to.be.equal('test-id');

    // Check the auto-generated id is unique.
    const autoIdInc = A11yAutocomplete(await fixture(`<input>`));
    expect(autoIdInc.id).to.be.equal('autocomplete-input-2');
  });

  it('term splitting with quotes', async () => {});

  it('autocomplete events', async () => {
    const input = await fixture(`<input>`);
    const count = {};
    const events = {
      change: [{}],
      close: [{}],
      created: [{}],
      destroy: [{}],
      highlight: [{ selected: 'two' }, { selected: 'three' }],
      open: [{}],
      'pre-search': [{}],
      response: [{ list: ['two', 'three'] }],
      select: [{ selected: 'three' }],
      'selection-added': [{ added: 'three' }],
    };
    Object.keys(events).forEach((name) => {
      count[name] = [];
      // eslint-disable-next-line no-unused-vars
      input.addEventListener(
        `autocomplete-${name}`,
        // ignore the autocomplete object, just look at the extra data we send
        // in the event.
        // eslint-disable-next-line no-unused-vars
        ({ detail: { autocomplete, ...rest } }) =>
          count[name].push({ ...rest }),
      );
    });

    // Create.
    const auto = A11yAutocomplete(input, { source: ['two', 'three'] });
    // Open, pre-search, response.
    await device.type(input, 't', { wait, clear: true });
    // Highlight.
    await device.press('ArrowDown');
    // Highlight another.
    await device.press('ArrowDown');
    // Select, change.
    await device.press('Enter');
    // No change.
    await device.press('Tab');
    // Destroy.
    auto.destroy();

    expect(count).to.be.eql(events);
  });

  it('keyboard suggestions navigation', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source });
    const wrap = expect(input.parentNode).dom;

    // Up/down
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown');
    await device.press('ArrowDown');
    await device.press('ArrowDown');
    await device.press('ArrowDown');
    await device.press('ArrowUp');
    await device.press('ArrowUp');
    await device.press('ArrowUp');
    await device.press('ArrowUp');
    await device.press('ArrowUp');
  });

  it('keyboard suggestions selection', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source });
    const wrap = expect(input.parentNode).dom;

    // Select value with enter.
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown');
    await device.press('Enter');
    expect(input.value).to.be.equal('two');
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;

    // Select value with space.
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown');
    await device.press('Space');
    expect(input.value).to.be.equal('two');
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
  });

  it('keyboard suggestion list close', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source });
    const wrap = expect(input.parentNode).dom;

    // Close suggestions with escape.
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown');
    await device.press('Escape');
    expect(input.value).to.be.equal('t');
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;

    // Close suggestions with tab.
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown');
    await device.press('Tab');
    expect(input.value).to.be.equal('t');
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
  });

  it('mouse click', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source });
    const wrap = expect(input.parentNode).dom;

    // Close suggestions with escape.
    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    const clickTarget = document.querySelector('[role="option"]');
    await device.click(clickTarget);
    expect(input.value).to.be.equal(clickTarget.innerText);
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
  });

  it('screenreader announcement highlighted', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source });
    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait, clear: true });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
    await device.press('ArrowDown', { wait: 550 });
    const text = document.querySelector(
      '[data-autocomplete-live-region]',
    ).innerText;
    expect(text).to.include('highlighted');
  });

  it('use existing assistive hint', async () => {});

  it('search result list and filter normalization', async () => {
    const data = [
      { value: 'one', label: 'one', group: 'Kitten' },
      { value: 'two', label: 'two', group: 'Kitten' },
      { value: 'three', group: 'Llama' },
      'twenty',
    ];
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source: data });
    const promise = new Promise((resolve) => {
      input.addEventListener(
        'autocomplete-response',
        ({ detail: { list } }) => {
          expect(list).to.be.eql([
            { value: 'two', label: 'two', group: 'Kitten' },
            { value: 'three', group: 'Llama' },
            'twenty',
          ]);
          resolve();
        },
      );
    });

    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait });
    wrap.have.descendants('[role="option"]').and.have.length(3);
    // Wait for the event to be triggered.
    return promise;
  });

  it('search by value', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source: data });
    const wrap = expect(input.parentNode).dom;

    await device.type(input, '1', { wait });
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;
    wrap.not.have.descendants('[role="option"]');
  });

  it('search by label', async () => {
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, { source: data });
    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="option"]').and.have.length(4);
  });

  it('option groupBy', async () => {
    const data = [
      { value: 'one', label: 'one', group: 'Kitten' },
      { value: 'two', label: 'two', group: 'Kitten' },
      { value: 'three', label: 'three', group: 'Llama' },
      { value: 'four', label: 'four', group: 'Llama' },
      // Item without group.
      { value: 'five', label: 'five' },
      // Item with group that contains HTML.
      {
        value: 'six',
        label: 'six',
        group: '<b>Elephant</b>',
      },
    ];
    const input = await fixture(`<input>`);
    A11yAutocomplete(input, {
      source: data,
      groupBy: 'group',
      cardinality: 3,
    });
    const wrap = expect(input.parentNode).dom;

    await device.type(input, 't', { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="group"]').and.have.length(2);
    wrap.have.descendants('[role="option"]').and.have.length(2);
    wrap.have.descendants('[id="autocomplete-l0-g0"]').and.have.length(1);
    wrap.have.descendants('[data-autocomplete-item="1"]').and.have.length(1);
    await device.press('ArrowDown');
    await device.press('ArrowDown');
    await device.press('Enter');
    expect(input.value).to.be.equal('three');

    await device.type(input, ', f', { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="group"]').and.have.length(1);
    wrap.have.descendants('[role="option"]').and.have.length(1);
    await device.press('ArrowDown');
    await device.press('Enter');
    expect(input.value).to.be.equal('three, four');
    wrap.have.descendant('[role="listbox"]').to.not.be.displayed;

    await device.type(input, ', s', { wait });
    wrap.have.descendant('[role="listbox"]').to.be.displayed;
    wrap.have.descendants('[role="group"]').and.have.length(1);
    wrap.have.descendants('[role="option"]').and.have.length(1);
    wrap.have.descendant('[role="presentation"] > b');
    await device.press('ArrowDown');
    await device.press('ArrowDown');
    await device.press('Enter');
    expect(input.value).to.be.equal('three, four, six');
  });

  it('option classes', async () => {
    const input = await fixture(`<input>`);
    const classes = {
      wrapper: 'auto-wrapper',
      input: 'auto-input',
      inputLoading: 'auto-inputLoading',
      listbox: 'auto-listbox',
      group: 'auto-group',
      groupLabel: 'auto-groupLabel',
      option: 'auto-option',
    };
    A11yAutocomplete(input, {
      source: (search, results) => {
        wrap.have.descendant('input').to.have.class('auto-inputLoading');
        results(data);
      },
      groupBy: 'group',
      classes,
    });
    const wrap = expect(input.parentNode).dom;

    wrap.to.have.class('auto-wrapper');
    wrap.have.descendant('input').to.have.class('auto-input');
    await device.type(input, 't', { wait: wait });
    wrap.have.descendant('input').to.not.have.class('auto-inputLoading');
    wrap.have.descendant('[role="listbox"]').to.have.class('auto-listbox');
    wrap.have.descendant('[role="option"]').to.have.class('auto-option');
    wrap.have.descendant('[role="group"]').to.have.class('auto-group');
    wrap.have
      .descendant('[role="group"] [role="presentation"]')
      .to.have.class('auto-groupLabel');
  });

  it('option classes data attributes', async () => {
    const input = await fixture(`<input
      data-autocomplete-classes-wrapper="auto-wrapper"
      data-autocomplete-classes-input="auto-input"
      data-autocomplete-classes-input-Loading="auto-inputLoading"
      data-autocomplete-classes-listbox="auto-listbox"
      data-autocomplete-classes-group="auto-group"
      data-autocomplete-classes-group-Label="auto-groupLabel"
      data-autocomplete-classes-option="auto-option"
    >`);
    A11yAutocomplete(input, {
      source: (search, results) => {
        wrap.have.descendant('input').to.have.class('auto-inputLoading');
        results(data);
      },
      groupBy: 'group',
    });
    const wrap = expect(input.parentNode).dom;

    wrap.to.have.class('auto-wrapper');
    wrap.have.descendant('input').to.have.class('auto-input');
    await device.type(input, 't', { wait: wait });
    wrap.have.descendant('input').to.not.have.class('auto-inputLoading');
    wrap.have.descendant('[role="listbox"]').to.have.class('auto-listbox');
    wrap.have.descendant('[role="option"]').to.have.class('auto-option');
    wrap.have.descendant('[role="group"]').to.have.class('auto-group');
    wrap.have
      .descendant('[role="group"] [role="presentation"]')
      .to.have.class('auto-groupLabel');
  });

  it('Test normalization pollution', async () => {
    const input = await fixture(`<input>`);
    const callback = ({ detail: { selected } }) => {
      expect(selected).to.be.eql('one');
    };

    const promises = Promise.all([
      new Promise((resolve) => {
        input.addEventListener(`autocomplete-select`, (e) => {
          callback(e);
          resolve();
        });
      }),
      new Promise((resolve) => {
        input.addEventListener(`autocomplete-highlight`, (e) => {
          callback(e);
          resolve();
        });
      }),
    ]);

    A11yAutocomplete(input, { source: list });
    await device.type(input, 'o', { wait, clear: true });
    await device.press('ArrowDown');
    await device.press('Enter');
    return promises;
  });
});
