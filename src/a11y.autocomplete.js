/**
 * A standalone autocomplete, optimized for accessibility.
 *
 * @module @drupal/autocomplete
 * @example @lang html <caption>Initialization</caption>
 * <!-- Ensure the assets are included in the page. -->
 * <link rel="stylesheet" href="[path]/a11y.autocomplete.css" />
 * <script src="[path]/a11y.autocomplete.js"></script>
 * <!-- Element to receive autocomplete functionality. -->
 * <input id="an-input" />
 *
 * <script>
 *   const input = document.querySelector('#an-input');
 *   // Initialize the autocomplete with a fixed list of items.
 *   const autocompleteInstanceFixedList = A11yAutocomplete(input, {
 *     source: ['first item', 'second item', 'third item'],
 *   });
 *   // Or initialize the autocomplete with dynamic results.
 *   const autocompleteInstanceDynamicList = A11yAutocomplete(input, {
 *     source: (query, result) => {
 *       result(['first item', 'second item', 'third item']);
 *     },
 *   });
 *   // When the autocomplete is initialized, markup is added.
 * </script>
 * @example @lang html <caption>HTML structure</caption>
 * <!-- The input is wrapped in a div, making is possible to position the results list with CSS. -->
 * <div data-drupal-autocomplete-wrapper>
 *   <!-- Several attributes are added to the input, used for accessibility and being identifiable by JavaScript. -->
 *   <input id="an-input" aria-autocomplete="list" autocomplete="off" data-drupal-autocomplete-input aria-owns="autocomplete-l0" role="combobox" aria-expanded="false" aria-describedby="assistive-hint-0">
 *     <!-- This span provides assistive technology such as screenreaders with additional information when the input is focused. -->
 *     <span class="visually-hidden" id="assistive-hint-0">Type 2 or more characters for results. When autocomplete results are available use up and down arrows to review and enter to select. Touch device users, explore by touch or with swipe gestures.</span>
 *     <!-- This is the <ul> that will list the query results when characters are typed in the input. -->
 *     <ul role="listbox" data-drupal-autocomplete-list="" id="autocomplete-l0" hidden=""></ul>
 *     <!-- This is a live region, used for conveying the results of interactions to assistive technology, such as the number of results available after typing. -->
 *     <span data-drupal-autocomplete-live-region="" aria-live="assertive"></span>
 * </div>
 * @example @lang html <caption>Setting Options</caption>
 * <!-- Options can be set in three ways, listed from highest precedence to lowest: -->
 *
 * <!-- 1. An object literal in the input's `data-autocomplete` attribute with the format {camelCaseOptionName: value}. -->
 * <input data-autocomplete='{"minChars": "3", "source":["first item", "second item", "third item"]}' />
 *
 * <!-- 2. Via the data-autocomplete-(hyphen delimited option name) attribute. -->
 * <input data-autocomplete-min-chars="3" data-autocomplete-source="['first item', 'second item', 'third item']" />
 *
 * <script>
 *   // 3. Via the options argument when initializing a new instance
 *   A11yAutocomplete(input, {minChars: 3, , source: ['first item', 'second item', 'third item']})
 * </script>
 */

/**
 * The API to manage an input's autocomplete functionality.
 *
 * This is part of the public API and triggers semver increment on change.
 *
 * @public
 * @typedef {object} A11yAutocomplete~Api
 * @prop {A11yAutocomplete~destroy} destroy
 *  Function to destroy the autocomplete instance.
 * @prop {string} id
 *  The id of the instance.
 */

/**
 * Default suggestion structure, one of `label` or `value` is required.
 *
 * This is the expected structure for a suggestion item. If the structure is
 * different make sure to override `options.templates.suggestion` and
 * `options.templates.inputValue` to generate the appropriate label and value
 * from the custom object structure.
 *
 * @public
 * @typedef {object} A11yAutocomplete~sourceObject
 * @prop {string} [label]
 *  The text used to show the suggestion in the dropdown.
 * @prop {string} [value]
 *  The text added to the input field upon selection.
 */

/**
 * Original source data.
 *
 * This data is passed directly to the {@link A11yAutocomplete~suggestionTemplate}
 * callback to generate the suggestion label displayed in the dropdown and the
 * {@link A11yAutocomplete~inputValueTemplate} callback to generate the
 * suggestion value that will be added to the input field upon selection.
 *
 * @public
 * @typedef {A11yAutocomplete~sourceObject|string} A11yAutocomplete~SourceData
 *  A single suggestion as received before normalization. By default it can be:
 *  - A simple string value
 *  - An object with a `label` and `value` property.
 *  @see A11yAutocomplete~suggestionTemplate
 *  @see A11yAutocomplete~inputValueTemplate
 */

/**
 * Structure of a normalized suggestion item.
 *
 * This is part of the public API and triggers semver increment on change.
 *
 * @public
 * @typedef {object} A11yAutocomplete~Suggestion
 * @prop {string} label
 *  Text or HTML used to display the item in the listbox. Result of the {@link
 *  A11yAutocomplete~Options.templates.suggestion} callback.
 * @prop {string} value
 *  Text added to the input field on selection. Result of the {@link
 *  A11yAutocomplete~Options.templates.inputValue} callback.
 * @prop {number} index
 *   When the suggestion list is displayed, an index is assigned to the
 *   suggestion to be able to know its place in the list of results.
 * @prop {A11yAutocomplete~SourceData} item
 *  Original object passed before normalization.
 */

/**
 * A callback to return autocomplete results dynamically.
 *
 * This is part of the public API and triggers semver increment on change.
 *
 * @public
 * @callback A11yAutocomplete~sourceCallback
 * @param {string} searchTerm
 *  The search term being searched for.
 * @param {A11yAutocomplete~sourceResultsCallback} results
 *  A callback to populate the results list.
 * @example @lang js <caption>Simple fetch call</caption>
 *  // Example querying an endpoint that provides JSON.
 *  source: (searchTerm, results) => {
 *    // Query the remote source
 *    fetch(`https://an-endpoint/?q=${searchTerm}`)
 *       .then((response) => response.json())
 *       // Use the callback to send the results to the
 *       .then(results);
 *  },
 */

/**
 * A callback that will display the results passed according to the configured
 * constraints.
 *
 * This is part of the public API and triggers semver increment on change.
 *
 * @public
 * @callback A11yAutocomplete~sourceResultsCallback
 * @param {A11yAutocomplete~SourceData[]} results
 *  A callback to populate the results list.
 */

/**
 * Options sent to the Autocomplete constructor that will override the default
 * options.
 *
 * This is part of the public API and triggers semver increment on change.
 *
 * @public
 * @typedef {object} A11yAutocomplete~Options
 * @prop {A11yAutocomplete~SourceData[]|A11yAutocomplete~sourceCallback} [source]
 *  The data the autocomplete searches, which can be provided in multiple ways:
 *  - An array of strings: `[ "First choice", "Second Choice", ... ]`
 *  - An array of objects. All objects must have either a 'label' or 'value'
 *    property: `[ { label: "First Choice", value: "First Value" }, ... ]`
 *    The label property is displayed in the suggestion list, and is what the
 *    autocomplete process searches. The value property is what is inserted into
 *    the input element when an item is selected. If just one property (value or
 *    label) is specified, it will be used for both, e.g., if only 'value' is
 *    provided, it will also be used as the label.
 *    Note that these objects can have additional properties beyond 'label' and
 *    'value', but at least one of those two properties must be present for the
 *    autocomplete to work.
 *  - A string of valid JSON that can be parsed into an array of strings or an
 *    array of objects (i.e. either of the array options listed above).
 *  - A callback function {@link A11yAutocomplete~sourceCallback}.
 * @prop {string} [groupBy]
 *  Name of the key used to group suggestions.
 * @prop {object} [templates]
 *  Defines templates (functions) that are used for displaying parts of the
 *  autocomplete.
 * @prop {A11yAutocomplete~suggestionTemplate} [templates.suggestions]
 *  Defines template for suggestion labels.
 * @prop {A11yAutocomplete~inputValueTemplate} [templates.inputValue]
 *  Defines template for suggestion input values.
 * @prop {object} [classes]
 * Defines classes to be added to the relevant Dom element.
 * @prop {string} [classes.wrapper]
 *  Defines classes for the autocomplete wrapper element.
 * @prop {string} [classes.input]
 *  Defines classes for the input field.
 * @prop {string} [classes.inputLoading]
 *  Defines classes for the input field applied while the source callback is in
 *  progress.
 * @prop {string} [classes.listbox]
 *  Defines classes for the suggestion item wrapper.
 * @prop {string} [classes.group]
 *  Defines classes for the group wrapper.
 * @prop {string} [classes.groupLabel]
 *  Defines classes for the item used as a group label.
 * @prop {string} [classes.option]
 *  Defines classes for the list item wrapper.
 * @prop {boolean|null} [allowRepeatValues=null]
 *  If `true`, autocomplete results can include items already included in the
 *  field. A null value functions the same as false, but a null value can be
 *  used to determine if this value was explicitly set or using defaults.
 * @prop {boolean} [autoFocus=false]
 *  When `true`, the first result is focused as soon as a list of results
 *  becomes visible.
 * @prop {string} [separatorChar=,]
 *  The character used to separate multiple values in the same form.
 * @prop {string} [firstCharacterIgnoreList=,]
 *  Any characters in this string will not be incorporated in a search as the
 *  first character of a query. Typically, this string should at least include
 *  the value of `separatorChar`.
 * @prop {number} [minChars=1]
 *  Minimum number of characters that must be typed before displaying
 *  autocomplete results.
 * @prop {number} [cardinality=1]
 *  The number of values the input can reference, where multiple values are
 *  separated by the character specified in the `separatorChar` option. Set to
 *  `-1` for unlimited cardinality.
 * @prop {boolean} [createLiveRegion=true]
 *  When `true`, initialization includes adding a live region specifically that
 *  will convey autocomplete activity to assistive technology. This is typically
 *  only set to `false` if a site has a centralized live region for assistive
 *  technology announcements.
 * @prop {string} [inputAssistiveHint=When autocomplete results are available use up and down arrows to review and enter to select. Touch device users, explore by touch or with swipe gestures.]
 *  Message conveyed to assistive technology when the input is focused.
 * @prop {string} [minCharAssistiveHint=Type @count or more characters for results]
 *  When `minChars` is greater than one, this is appended to
 *  `inputAssistiveHint` so users are aware how many characters are
 *  needed to trigger a search.  `@count` is replaced with the value of`minChars`.
 * @prop {string} [noResultsAssistiveHint=No results found]
 *  Message conveyed to assistive technology when the query returns no results.
 * @prop {string} [someResultsAssistiveHint=There are @count results available.]
 *  Message conveyed to assistive technology when the number of results exceeds
 *  one. `@count` is replaced with the number of results returned.
 * @prop {string} [oneResultAssistiveHint=There is one result available.]
 *  Message conveyed to assistive technology when there is one result.
 * @prop {string} [highlightedAssistiveHint=@selectedItem @position of @count is highlighted]
 *  Message conveyed to assistive technology when a result is selected from the
 *  list.
 */

/**
 * Base class
 *
 * @private
 */
class _A11yAutocomplete {
  /**
   * Construct a new A11yAutocomplete class.
   *
   * @param {HTMLInputElement} input
   *   The element to be used as an autocomplete.
   * @param {A11yAutocomplete~Options} options
   *  Autocomplete options.
   */
  constructor(input, options = {}) {
    this.keyCode = Object.freeze({
      TAB: 9,
      RETURN: 13,
      ESC: 27,
      SPACE: 32,
      PAGEUP: 33,
      PAGEDOWN: 34,
      END: 35,
      HOME: 36,
      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40,
    });

    /**
     * Copy of the original input to be able to restore on destroy.
     *
     * @private
     * @type {Node}
     */
    this.originalInput = input.cloneNode();
    /**
     * Current input.
     *
     * @private
     * @type {HTMLInputElement}
     */
    this.input = input;
    /**
     * Value used to create a unique id on the page.
     *
     * @private
     * @type {number}
     */
    this.count = document.querySelectorAll('[data-autocomplete-input]').length;
    /**
     * The unique HTML id for this autocomplete elements.
     *
     * @private
     * @type {string}
     */
    this.listboxId = `autocomplete-l${this.count}`;
    /**
     * A list of allowed configuration options.
     *
     * @private
     * @type {string[]}
     */
    this.supportedOptions = [
      'source',
      'cardinality',
      'minChars',
      'separatorChar',
      'firstCharacterIgnoreList',
      'createLiveRegion',
      'autoFocus',
      'allowRepeatValues',
      'groupBy',
      'templates',
      'classes',
      // messages.
      'minCharAssistiveHint',
      'inputAssistiveHint',
      'noResultsAssistiveHint',
      'someResultsAssistiveHint',
      'oneResultAssistiveHint',
      'highlightedAssistiveHint',
    ];

    /**
     * Set of default options.
     *
     * @private
     * @type {A11yAutocomplete~Options}
     */
    this.defaultOptions = {
      // from jquery ui
      autoFocus: false,
      firstCharacterIgnoreList: ',',
      minChars: 1,
      // API already sort results
      sort: false,
      // shim
      disabled: false,
      source: [],
      groupBy: null,
      cardinality: 1,
      separatorChar: ',',
      // for drupal
      createLiveRegion: true,
      // shim
      listZindex: 100,
      // to pass jquery ui tests
      allowRepeatValues: null,
      searchDelay: 300,
      templates: {
        /**
         * Formats how a suggestion is structured in the suggestion list.
         *
         * @callback A11yAutocomplete~suggestionTemplate
         * @param {A11yAutocomplete~SourceData} item
         *   Item object from the source parameter.
         * @return {string}
         *   The text and html of a suggestion item.
         */
        suggestion: (item) => (item.label || item.value || item).trim(),
        /**
         * Format the text that will be added to the input field.
         *
         * @callback A11yAutocomplete~inputValueTemplate
         * @param {A11yAutocomplete~SourceData} item
         *   Item object from the source parameter.
         * @return {string}
         *   The text to add to the input field value.
         */
        inputValue: (item) => item.value || item.label || item,
      },
      classes: {
        wrapper: '',
        input: '',
        inputLoading: '',
        listbox: '',
        group: '',
        groupLabel: '',
        option: '',
      },
      // to nest later.
      minCharAssistiveHint: 'Type @count or more characters for results',
      inputAssistiveHint:
        'When autocomplete results are available use up and down arrows to review and enter to select. Touch device users, explore by touch or with swipe gestures.',
      noResultsAssistiveHint: 'No results found',
      someResultsAssistiveHint: 'There are @count results available.',
      oneResultAssistiveHint: 'There is one result available.',
      highlightedAssistiveHint:
        '@selectedItem @position of @count is highlighted',
    };

    /**
     * Initialized options for this autocomplete instance.
     *
     * @private
     * @type {A11yAutocomplete~Options}
     */
    this.options = this.initOptions(
      this.filterOptions(options),
      this.filterOptions(this.attributesToOptions()),
    );

    // Preset lists provided as strings should be converted to options.
    if (typeof this.options.source === 'string') {
      this.options.source = JSON.parse(this.options.source);
    }

    if (
      typeof this.options.source !== 'function' &&
      !Array.isArray(this.options.source)
    ) {
      throw new TypeError('options.source is not an array or a function.');
    }

    /**
     * Currently selected suggestion.
     *
     * @type {A11yAutocomplete~SourceData|null}
     */
    this.selected = null;
    this.preventCloseOnBlur = false;
    this.isOpened = false;
    /**
     * The list of suggestions displayed in the listbox.
     *
     * @type {A11yAutocomplete~Suggestion[]}
     */
    this.suggestions = [];
    this.announceTimeOutId = null;
    this.searchTimeOutId = null;

    // Create a div that will wrap the input and suggestion list.
    this.wrapper = document.createElement('div');
    this.implementWrapper();
    this.implementInput();
    /**
     * The unique HTML id for the autocomplete assistive description hint.
     *
     * It is removed after it is read once.
     *
     * @private
     * @type {string}
     */
    this.assistiveHintId = `${this.input.id}-assistive-hint`;
    this.inputHintRead = false;
    this.implementDescription();

    // Create the list that will display suggestions.
    this.listboxWrapper = document.createElement(
      this.options.groupBy ? 'div' : 'ul',
    );
    this.implementList();
    this.appendList();

    // When applicable, create a live region for announcing suggestion results
    // to assistive technology.
    this.liveRegion = null;
    this.implementLiveRegion();

    /**
     * Helper to facilitate event binding.
     *
     * @private
     * @type {object}
     */
    this.events = {
      input: {
        input: (e) => this.inputListener(e),
        blur: (e) => this.blurHandler(e),
        keydown: (e) => this.inputKeyDown(e),
      },
      listboxWrapper: {
        mousedown: (e) => e.preventDefault(),
        click: (e) => this.itemClick(e),
        keydown: (e) => this.listKeyDown(e),
        blur: (e) => this.blurHandler(e),
        focus: (e) => this.listFocus(e),
      },
    };

    Object.keys(this.events).forEach((elementName) => {
      Object.keys(this.events[elementName]).forEach((eventName) => {
        this[elementName].addEventListener(
          eventName,
          this.events[elementName][eventName],
        );
      });
    });

    /**
     * User-facing autocomplete API
     *
     * @public
     * @type {A11yAutocomplete~Api}
     */
    this.api = {
      destroy: this.destroy.bind(this),
      id: this.input.id,
      /**
       * Do not use! Only for jquery shim.
       *
       * @deprecated
       * @private
       */
      _internal_object: this,
    };

    /**
     * Fires after initialization and markup additions.
     *
     * @event A11yAutocomplete#autocomplete-created
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     */
    this.triggerEvent('autocomplete-created');
  }

  /**
   * Only accept a subset of supported options.
   *
   * @private
   * @param {A11yAutocomplete~Options} options
   *  An object to be checked for supported options.
   * @return {A11yAutocomplete~Options}
   *  The filtered set of options recognized by the library.
   */
  filterOptions(options) {
    const filteredOptions = {};
    const rejectedOptions = {};
    // Later loop on the supported options array instead,
    // need to see rejected options to help debug jquery shim for now.
    Object.keys(options).forEach((key) => {
      if (this.supportedOptions.includes(key)) {
        filteredOptions[key] = options[key];
      } else {
        rejectedOptions[key] = options[key];
      }
    });
    // Temporary to help debug the shim.
    if (Object.keys(rejectedOptions).length) {
      console.warn('Rejected autocomplete options: ', rejectedOptions);
    }
    return filteredOptions;
  }

  /**
   * Helper to merge a nested property
   *
   * @private
   * @param {"templates"|"classes"} name
   *  Name of the nested option key
   * @param {A11yAutocomplete~Options} options
   *  A list of option object to merge.
   * @return {A11yAutocomplete~Options.templates|A11yAutocomplete~Options.classes}
   *  The merged result of all the options objects.
   */
  mergeNestedOptions(name, ...options) {
    let result = {};
    options.forEach((optionType) => {
      if (optionType && optionType[name]) {
        result = { ...result, ...optionType[name] };
      }
    });
    return result;
  }

  /**
   * Merge all options handling nested values.
   *
   * @private
   * @param {A11yAutocomplete~Options} options
   *  A list of option objects to merge.
   * @return {A11yAutocomplete~Options}
   *  Resulting configuration including merging of nested keys `templates` and
   *  `classes`.
   */
  initOptions(...options) {
    const defaultOptions = this.defaultOptions;
    /** @type {A11yAutocomplete~Options} */
    let result = {};
    const templates = this.mergeNestedOptions(
      'templates',
      defaultOptions,
      ...options,
    );
    const classes = this.mergeNestedOptions(
      'classes',
      defaultOptions,
      ...options,
    );
    options.forEach((option) => {
      result = { ...result, ...option };
    });
    return {
      ...defaultOptions,
      ...result,
      ...{ templates, classes },
    };
  }

  /**
   * Sets attributes to the wrapper and inserts it in the DOM.
   *
   * @private
   */
  implementWrapper() {
    this.applyClasses('wrapper');
    this.wrapper.setAttribute('data-autocomplete-wrapper', '');
    // Append the wrapper in place of the input and add the input inside the
    // wrapper.
    // Before:
    //  <form>
    //    <input>
    //  </form>
    //
    // After:
    //  <form>
    //    <div data-autocomplete-wrapper><input></div>
    //  </form>
    // Add the wrapper immediately before the input element.
    this.input.parentNode.insertBefore(this.wrapper, this.input);
    this.wrapper.appendChild(this.input);
  }

  /**
   * Sets attributes to the input and inserts it in the DOM.
   *
   * @private
   */
  implementInput() {
    // Add attributes to the input.
    this.applyClasses('input');
    this.input.setAttribute('aria-autocomplete', 'list');
    this.input.setAttribute('autocomplete', 'off');
    this.input.setAttribute('data-autocomplete-input', '');
    this.input.setAttribute('aria-owns', this.listboxId);
    this.input.setAttribute('role', 'combobox');
    this.input.setAttribute('aria-expanded', 'false');
    if (!this.input.hasAttribute('id')) {
      this.input.setAttribute('id', `autocomplete-input-${this.count}`);
    }
  }

  /**
   * Adds assistive hints.
   *
   * @private
   */
  implementDescription() {
    const description = document.createElement('span');
    description.textContent =
      this.minCharsMessage() + this.options.inputAssistiveHint;
    description.classList.add('visually-hidden');
    description.setAttribute('id', this.assistiveHintId);
    this.wrapper.appendChild(description);

    // Update the aria-describedby attribute and add a reference to the newly
    // created description.
    const describedBy = this.input.getAttribute('aria-describedby') || '';
    const describedByValue = `${describedBy} ${this.assistiveHintId}`
      .replace(/\s+/g, ' ')
      .trim();
    // Use a reference list to point to all the descriptions necessary.
    this.input.setAttribute('aria-describedby', describedByValue);
  }

  /**
   * Inserts list into DOM.
   *
   * @private
   */
  appendList() {
    this.input.parentNode.appendChild(this.listboxWrapper);
  }

  /**
   * Sets attributes to the results list and inserts it in the DOM.
   *
   * @private
   */
  implementList() {
    this.applyClasses('listbox');
    this.listboxWrapper.setAttribute('role', 'listbox');
    this.listboxWrapper.setAttribute('data-autocomplete-item-list', '');
    this.listboxWrapper.setAttribute('id', this.listboxId);
    this.listboxWrapper.setAttribute('hidden', '');
  }

  /**
   * Creates a live region for reporting status to assistive technology.
   *
   * @private
   */
  implementLiveRegion() {
    // If the liveRegion option is set to true, create a new live region and
    // insert it in the autocomplete wrapper.
    if (this.options.createLiveRegion === true) {
      this.liveRegion = document.createElement('span');
      this.liveRegion.setAttribute('data-autocomplete-live-region', '');
      this.liveRegion.setAttribute('aria-live', 'assertive');
      this.input.parentNode.appendChild(this.liveRegion);
    }

    // If the liveRegion option is a string, it should be a selector for an
    // already-existing live region.
    if (typeof this.options.liveRegion === 'string') {
      this.liveRegion = document.querySelector(this.options.liveRegion);
    }
  }

  /**
   * Converts data-autocomplete* attributes into options.
   *
   * @private
   * @return {A11yAutocomplete~Options}
   *  An autocomplete options object.
   */
  attributesToOptions() {
    const options = { classes: {} };
    // Any options provided in the `data-autocomplete` attribute will take
    // precedence over those specified in `data-autocomplete-(x)`.
    const dataAutocompleteAttributeOptions = this.input.dataset.autocomplete
      ? JSON.parse(this.input.dataset.autocomplete)
      : {};
    const dataset = this.input.dataset;
    // Loop through all of the input's attributes. Any attributes beginning with
    // `data-autocomplete` will be added to an options object.
    for (const key in dataset) {
      if (
        key.includes('autocomplete') &&
        key !== 'autocomplete' &&
        key !== 'autocompleteInput'
      ) {
        let optionName = key.replace('autocomplete', '');
        optionName = optionName.charAt(0).toLowerCase() + optionName.slice(1);
        const value = dataset[key];
        if (['true', 'false'].includes(value)) {
          options[optionName] = value === 'true';
        } else if (['cardinality', 'minChars'].includes(optionName)) {
          // Transform strings to number
          options[optionName] = +value;
        } else if (/^classes\w+$/.test(optionName)) {
          // Handle nested properties for the classes options.
          optionName = optionName.replace('classes', '');
          optionName = optionName.charAt(0).toLowerCase() + optionName.slice(1);
          options.classes[optionName] = value;
        } else {
          options[optionName] = value;
        }
      }
    }
    return { ...options, ...dataAutocompleteAttributeOptions };
  }

  /**
   * Handles blur events.
   *
   * @private
   * @param {Event} e
   *   The blur event.
   */
  blurHandler(e) {
    // If an element is blurred, cancel any pending screenreader announcements
    // as they would be specific to an element no longer in focus.
    window.clearTimeout(this.announceTimeOutId);
    if (this.preventCloseOnBlur) {
      this.preventCloseOnBlur = false;
      e.preventDefault();
    } else {
      /**
       * Fires after an item is blurred and another item hasn't been
       * highlighted.
       *
       * @event A11yAutocomplete#autocomplete-change
       * @type {CustomEvent}
       * @prop {object} detail
       *  Detail property of the event.
       * @prop {A11yAutocomplete~Api} detail.autocomplete
       *  The autocomplete instance.
       */
      // Only fire `autocomplete-change` when the element that was blurred
      // is an autocomplete item. `blurHandler()` is also attached to
      // the `input`, and moving focus away from the `input` should not fire
      // `autocomplete-change`.
      if (e.target.closest('[data-autocomplete-item]')) {
        this.triggerEvent('autocomplete-change');
        this.close();
      }
    }
  }

  /**
   * Removes one-time-only assistive hints.
   *
   * @private
   */
  removeAssistiveHint() {
    if (!this.inputHintRead) {
      const hint = document.querySelector(`[id="${this.assistiveHintId}"]`);
      if (hint && hint.parentNode) {
        hint.parentNode.removeChild(hint);
      }

      // Remove the reference to the deleted element from the aria-describedBy
      // list
      const describedBy = this.input.getAttribute('aria-describedby') || '';
      const describedByValue = describedBy
        .replace(this.assistiveHintId, '')
        .replace(/\s+/g, ' ')
        .trim();
      if (describedByValue === '') {
        this.input.removeAttribute('aria-describedby');
      } else {
        // Use a reference list to point to all the descriptions necessary.
        this.input.setAttribute('aria-describedby', describedByValue);
      }
      this.inputHintRead = true;
    }
  }

  /**
   * Handles keydown events on the item list.
   *
   * @private
   * @param {KeyboardEvent} e
   *   The keydown event.
   */
  listKeyDown(e) {
    if (
      !this.listboxWrapper.contains(document.activeElement) ||
      e.ctrlKey ||
      e.altKey ||
      e.metaKey ||
      e.keyCode === this.keyCode.TAB
    ) {
      return;
    }

    this.listboxWrapper
      .querySelectorAll('[aria-selected="true"]')
      .forEach((li) => {
        li.setAttribute('aria-selected', 'false');
      });

    switch (e.keyCode) {
      case this.keyCode.SPACE:
      case this.keyCode.RETURN:
        this.selectItem(document.activeElement, e);
        this.close();
        this.input.focus();
        break;

      case this.keyCode.ESC:
      case this.keyCode.TAB:
        this.input.focus();
        this.close();
        break;

      case this.keyCode.UP:
        this.focusPrev();
        break;

      case this.keyCode.DOWN:
        this.focusNext();
        break;

      default:
        break;
    }

    e.stopPropagation();
    e.preventDefault();
  }

  /**
   * Handles focus events on the item list.
   *
   * @private
   * @param {Event} e
   *   The focus event.
   */
  // eslint-disable-next-line no-unused-vars, class-methods-use-this
  listFocus(e) {
    // Intentionally empty, can be overridden.
  }

  /**
   * Moves focus to the previous list item.
   *
   * @private
   */
  focusPrev() {
    this.preventCloseOnBlur = true;
    const currentItem = document.activeElement.getAttribute(
      'data-autocomplete-item',
    );
    const prevIndex = parseInt(currentItem, 10) - 1;
    const previousItem = this.listboxWrapper.querySelector(
      `[data-autocomplete-item="${prevIndex}"]`,
    );

    if (previousItem) {
      this.highlightItem(previousItem);
    } else {
      this.input.focus();
    }
  }

  /**
   * Moves focus to the next list item.
   *
   * @private
   */
  focusNext() {
    const currentItem = document.activeElement.getAttribute(
      'data-autocomplete-item',
    );
    const nextIndex = parseInt(currentItem, 10) + 1;
    const nextItem = this.listboxWrapper.querySelector(
      `[data-autocomplete-item="${nextIndex}"]`,
    );
    if (nextItem) {
      this.preventCloseOnBlur = true;
      this.highlightItem(nextItem);
    }
  }

  /**
   * Highlights and focuses a selected item.
   *
   * @private
   * @param {HTMLElement} item
   *   The list item being selected.
   */
  highlightItem(item) {
    item.setAttribute('aria-selected', 'true');
    item.focus();
    const itemIndex = item
      .closest('[data-autocomplete-item]')
      .getAttribute('data-autocomplete-item');

    /**
     * Fires when an item is highlighted.
     *
     * @event A11yAutocomplete#autocomplete-highlight
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     * @prop {A11yAutocomplete~SourceData} detail.selected
     *  The original item as provided in the source.
     */

    /** @type {A11yAutocomplete~Suggestion} */
    const suggestion = this.suggestions[itemIndex];
    this.triggerEvent('autocomplete-highlight', { selected: suggestion.item });
    this.announceHighlight(item);
  }

  /**
   * Announces to assistive tech when an item is highlighted.
   *
   * @private
   * @param {HTMLElement} item
   *   The list item being selected.
   */
  announceHighlight(item) {
    window.clearTimeout(this.announceTimeOutId);
    // Delay the announcement by 500 milliseconds. This prevents unnecessary
    // calls when a user is navigating quickly.
    this.announceTimeOutId = setTimeout(
      () => this.sendToLiveRegion(this.highlightMessage(item)),
      500,
    );
  }

  /**
   * A message announced when an item is highlighted.
   *
   * @private
   * @param {HTMLElement} item
   *  The list item being highlighted.
   * @return {string}
   *  The message conveying that the item is highlighted.
   */
  highlightMessage(item) {
    const itemIndex = item
      .closest('[data-autocomplete-item]')
      .getAttribute('data-autocomplete-item');
    const selectedItem = this.suggestions[itemIndex].value;
    return this.options.highlightedAssistiveHint
      .replace('@selectedItem', selectedItem)
      .replace('@position', item.getAttribute('aria-posinset'))
      .replace('@count', this.suggestions.length);
  }

  /**
   * Handles keydown events on the autocomplete input.
   *
   * @private
   * @param {KeyboardEvent} e
   *   The keydown event.
   */
  inputKeyDown(e) {
    const { keyCode } = e;
    if (this.isOpened) {
      if (keyCode === this.keyCode.ESC) {
        this.close();
      }
      if (keyCode === this.keyCode.DOWN) {
        e.preventDefault();
        this.preventCloseOnBlur = true;
        this.highlightItem(
          this.listboxWrapper.querySelector('[role="option"]'),
        );
      }
    }
    this.removeAssistiveHint();
  }

  /**
   * Handles click events on the item list.
   *
   * @private
   * @param {MouseEvent} e
   *   The click event.
   */
  itemClick(e) {
    const li = e.target;

    if (li && e.button === 0) {
      this.selectItem(li, e);
    }
  }

  /**
   * Selects an item in the autocomplete list.
   *
   * @private
   * @param {Element} elementWithItem
   *  The element containing the item.
   * @param {Event} e
   *  The event that triggered te selection.
   */
  selectItem(elementWithItem, e) {
    const itemIndex = elementWithItem
      .closest('[data-autocomplete-item]')
      .getAttribute('data-autocomplete-item');
    const { item } = this.suggestions[itemIndex];

    /**
     * Fires when an item is selected for addition. This event can be canceled
     * and prevent the addition of the item.
     *
     * @event A11yAutocomplete#autocomplete-select
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     * @prop {A11yAutocomplete~SourceData} detail.selected
     *  The original item as provided in the source.
     */

    /** @type {boolean} */
    let selected = this.triggerEvent(
      'autocomplete-select',
      {
        selected: item,
      },
      true,
      e,
    );
    if (selected) {
      this.replaceInputValue(elementWithItem);
      e.preventDefault();
      this.close();
      /**
       * Fires after an item is added to the autocomplete
       *
       * @event A11yAutocomplete#autocomplete-selection-added
       * @type {CustomEvent}
       * @prop {object} detail
       *  Detail property of the event.
       * @prop {A11yAutocomplete~Api} detail.autocomplete
       *  The autocomplete instance.
       * @prop {string} detail.added
       *  The text added to the input value.
       */
      this.triggerEvent('autocomplete-selection-added', {
        added: elementWithItem.textContent,
      });
    }
  }

  /**
   * Replaces the value of an input field when a new value is chosen.
   *
   * @private
   * @param {Element} element
   *   The element with the item to be added.
   */
  replaceInputValue(element) {
    const itemIndex = element
      .closest('[data-autocomplete-item]')
      .getAttribute('data-autocomplete-item');
    /** @type {A11yAutocomplete~Suggestion} */
    const { value, item } = this.suggestions[itemIndex];
    this.selected = item;
    const separator = this.separator();
    if (separator.length > 0) {
      const before = this.previousItems(separator);
      this.input.value = `${before}${value}`;
    } else {
      this.input.value = value;
    }
  }

  /**
   * Returns the separator character.
   *
   * @private
   * @return {string}
   *  The separator character or a zero-length string.
   *  If the autocomplete input does not support multiple items or has reached
   *  The maximum number of items that can be added, a zero-length string is
   *  returned as no separator is needed.
   */
  separator() {
    const { cardinality } = this.options;
    const numItems = this.splitValues().length - 1;
    return numItems < cardinality || cardinality <= 0
      ? this.options.separatorChar
      : '';
  }

  /**
   * Gets all existing items in the autocomplete input.
   *
   * @private
   * @param {string} separator
   *   The character separating the items.
   * @return {string}
   *   The string of existing values in the input.
   */
  previousItems(separator) {
    // Needs some explanations for the regex here.
    const escapedSeparator = separator.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    const regex = new RegExp(`^.+${escapedSeparator}\\s*|`);
    const match = this.inputValue().match(regex)[0];
    return match && match.length > 0 ? `${match.trim()} ` : '';
  }

  /**
   * Take classes from the options and apply it to the corresponding DOM
   * element.
   *
   * Internal function to simplify code in Drupal core jQuery UI autocomplete
   * shim.
   *
   * @private
   * @param {string} element
   *  One of 'input', 'listbox', 'wrapper'.
   */
  applyClasses(element) {
    const { classes } = this.options;
    let domElement = this[element];
    // Temporary fix for matching the right elements.
    if (element === 'listbox') {
      domElement = this.listboxWrapper;
    }
    if (classes[element] && domElement && domElement instanceof Element) {
      this.addClasses(domElement, classes[element]);
    }
  }

  /**
   * Helper method to support IE11 to add classes to an element.
   *
   * @private
   * @param {Element} element
   *  The Element to add classes to.
   * @param {string} classes
   *  A space separated list of classes to add to the element.
   */
  addClasses(element, classes) {
    classes &&
      classes
        .split(' ')
        .forEach((className) => element.classList.add(className));
  }

  /**
   * Helper method to support IE11 to remove classes from an element.
   *
   * @private
   * @param {Element} element
   *  The Element to add classes to.
   * @param {string} classes
   *  A space separated list of classes to remove from the element.
   */
  removeClasses(element, classes) {
    classes &&
      classes
        .split(' ')
        .forEach((className) => element.classList.remove(className));
  }

  /**
   * Triggers an autocomplete search.
   *
   * @private
   * @param {Event} e
   *   The event triggering the search.
   */
  doSearch(e) {
    const { disabled, minChars, firstCharacterIgnoreList, cardinality } =
      this.options;
    if (disabled) {
      this.close();
      return;
    }
    const searchTerm = this.extractLastInputValue();
    if (searchTerm && searchTerm.length < minChars) {
      this.displayResults([]);
      return;
    }
    // The term starts with an ignored character, do not trigger a search.
    if (searchTerm && firstCharacterIgnoreList.includes(searchTerm[0])) {
      this.displayResults([]);
      return;
    }
    // We reached the maximum number of items, do not trigger a search.
    const currentValues = this.splitValues();
    if (cardinality > 0 && currentValues.length > cardinality) {
      this.displayResults([]);
      return;
    }

    /**
     * Fires just before a search. Can be used to cancel the search.
     *
     * @event A11yAutocomplete#autocomplete-pre-search
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     */
    if (!this.triggerEvent('autocomplete-pre-search', {}, true, e)) {
      return;
    }

    if (searchTerm && searchTerm.length > 0) {
      if (typeof this.options.source === 'function') {
        this.addClasses(this.input, this.options.classes.inputLoading);
        this.options.source(searchTerm, (results) => {
          this.removeClasses(this.input, this.options.classes.inputLoading);

          /**
           * Fires after suggestion items are retrieved, but before they are added to
           * the DOM.
           *
           * @event A11yAutocomplete#autocomplete-response
           * @type {CustomEvent}
           * @prop {object} detail
           *  Detail property of the event.
           * @prop {A11yAutocomplete~Api} detail.autocomplete
           *  The autocomplete instance.
           * @prop {A11yAutocomplete~Suggestion[]} detail.list
           *  The normalized array of suggestions to display.
           */
          this.triggerEvent('autocomplete-response', { list: results });
          const normalizedResults = this.normalizeSuggestionItems(results);
          const constrainedResults = this.applySelectionConstraints(
            normalizedResults,
            this.splitValues(),
          );
          this.displayResults(constrainedResults);
        });
      } else if (Array.isArray(this.options.source)) {
        const filteredResults = this.options.source.filter((item) =>
          this.filterResults(item, searchTerm),
        );

        /**
         * Fires after suggestion items are retrieved, but before they are added to
         * the DOM.
         *
         * @event A11yAutocomplete#autocomplete-response
         * @type {CustomEvent}
         * @prop {object} detail
         *  Detail property of the event.
         * @prop {A11yAutocomplete~Api} detail.autocomplete
         *  The autocomplete instance.
         * @prop {A11yAutocomplete~Suggestion[]} detail.list
         *  The normalized array of suggestions to display.
         */
        this.triggerEvent('autocomplete-response', { list: filteredResults });
        const normalizedResults =
          this.normalizeSuggestionItems(filteredResults);
        const constrainedResults = this.applySelectionConstraints(
          normalizedResults,
          this.splitValues(),
        );
        // If a predefined list was provided as an option, make this the
        // suggestion items.
        this.displayResults(constrainedResults);
      } else {
        throw new TypeError('options.source is not an array or a function.');
      }
    } else {
      // If the search query is empty, provide an empty list of suggestions.
      this.displayResults([]);
    }
  }

  /**
   * Takes input events and has them trigger searches when appropriate.
   *
   * @private
   * @param {InputEvent} e
   *   The input event.
   */
  inputListener(e) {
    if (!this.searchTimeOutId || this.options.searchDelay === 0) {
      this.searchTimeOutId = setTimeout(() => {
        this.doSearch(e);
        this.searchTimeOutId = null;
      }, this.options.searchDelay);
    }
  }

  /**
   * Converts all suggestions into an object with value and label properties.
   *
   * This should only be creating the label and value properties using
   * {@link A11yAutocomplete~suggestionTemplate} and
   * {@link A11yAutocomplete~inputValueTemplate}. There is also a jquery ui
   * normalization feature used for backwards compatibility in Drupal core.
   *
   * @private
   * @param {object[]|string[]} suggestionItems
   *  An array of suggestion items to be normalized.
   * @return {A11yAutocomplete~Suggestion[]}
   *  An array of normalized suggestion items.
   */
  normalizeSuggestionItems(suggestionItems) {
    const {
      templates: { suggestion, inputValue },
    } = this.options;
    return suggestionItems.map((item, index) => ({
      label: suggestion(item),
      value: inputValue(item),
      index,
      item,
    }));
  }

  /**
   * Displays the results retrieved in inputListener().
   *
   * @private
   * @param {A11yAutocomplete~Suggestion[]} suggestionItems
   *  A normalized list of suggestions to display.
   */
  displayResults(suggestionItems) {
    this.suggestions = suggestionItems;
    this.listboxWrapper.innerHTML = '';
    if (this.suggestions.length) {
      if (this.options.sort !== false) {
        this.suggestions = this.sortSuggestions(this.suggestions);
      }

      const fragment = document.createDocumentFragment();
      const appendToFragment = fragment.appendChild.bind(fragment);
      const suggestionItem = this.suggestionItem.bind(this);
      const groupBy = this.options.groupBy;
      if (groupBy) {
        // Group suggestions in an object with the key as the group label.
        Object.values(
          this.suggestions.reduce(this.groupByCallback(groupBy), {}),
        )
          .map(({ label, index, suggestions }) => {
            const groupId = `${this.listboxId}-g${index}`;
            const groupEl = this.suggestionGroup(label, groupId);
            const appendToGroup = groupEl.appendChild.bind(groupEl);
            suggestions.map(suggestionItem).forEach(appendToGroup);
            return groupEl;
          })
          .forEach(appendToFragment);
      } else {
        this.suggestions.map(suggestionItem).forEach(appendToFragment);
      }

      this.listboxWrapper.appendChild(fragment);
    }
    if (this.suggestions.length === 0) {
      this.close();
    } else {
      this.open();
    }

    window.clearTimeout(this.announceTimeOutId);
    // Delay the results announcement by 1400 milliseconds. This prevents
    // unnecessary calls when a user is typing quickly, and avoids the results
    // announcement being cut short by the screenreader stating the just-typed
    // character.
    this.announceTimeOutId = setTimeout(
      () => this.sendToLiveRegion(this.resultsMessage(this.suggestions.length)),
      1400,
    );
  }

  /**
   * Sorts the array of suggestions.
   *
   * @private
   * @param {A11yAutocomplete~Suggestion[]} suggestionItems
   *   The current suggestions.
   * @return {A11yAutocomplete~Suggestion[]}
   *   Ordered list of suggestions.
   */
  sortSuggestions(suggestionItems) {
    return suggestionItems.sort((prior, current) =>
      prior.label.toUpperCase() > current.label.toUpperCase() ? 1 : -1,
    );
  }

  /**
   * Creates a function used for a .map callback.
   *
   * @private
   * @param {string} groupBy
   *  The name of the property used to group suggestions.
   * @return {Function}
   *  A callback to be used in {@link A11yAutocomplete#displayResults}
   */
  groupByCallback(groupBy) {
    /**
     * Keep track of the index for each suggestion. It is necessary to reindex
     * suggestions because some might be removed from the set if they can not
     * be grouped.
     *
     * @type {number}
     */
    let suggestionIndex = 0;
    /**
     * Keep track of the group index to generate HTML id.
     *
     * @type {number}
     */
    let groupIndex = 0;
    /**
     * Callback used in a reduce function.
     *
     * @private
     * @callback A11yAutocomplete~groupCallback
     * @param {object} groups
     *  The group aggregates.
     * @param {A11yAutocomplete~Suggestion} suggestion
     *  The current suggestion to process.
     * @return {{label: string, index: number, suggestions:
     *   A11yAutocomplete~Suggestion[]}} Grouped suggestions.
     */
    return (groups, suggestion) => {
      const groupLabel = suggestion.item[groupBy];
      // Items without group key are not rendered.
      if (!groupLabel) {
        return groups;
      }
      suggestion.index = suggestionIndex++;
      if (!groups[groupLabel]) {
        groups[groupLabel] = {
          label: groupLabel,
          index: groupIndex++,
          suggestions: [],
        };
      }
      groups[groupLabel].suggestions.push(suggestion);
      return groups;
    };
  }

  /**
   * Creates a suggestion group.
   *
   * @private
   * @param {string} group
   *   A string containing HTML or text for the label of the group.
   * @param {string} id
   *   A unique ID for the group.
   * @return {HTMLElement}
   *   A list representing a group.
   */
  suggestionGroup(group, id) {
    const ul = document.createElement('ul');
    ul.setAttribute('role', 'group');
    ul.setAttribute('aria-labelledby', id);
    const li = document.createElement('li');
    li.innerHTML = group;
    li.setAttribute('role', 'presentation');
    li.setAttribute('id', id);
    ul.appendChild(li);

    this.addClasses(ul, this.options.classes.group);
    this.addClasses(li, this.options.classes.groupLabel);
    return ul;
  }

  /**
   * Creates a list item that displays the suggestion.
   *
   * @private
   * @param {A11yAutocomplete~Suggestion} suggestion
   *   A suggestion based on user input. It is an object with label and value
   *   properties.
   * @return {HTMLElement}
   *   A list item with the suggestion.
   */
  suggestionItem({ label, index }) {
    const li = document.createElement('li');
    li.innerHTML = label;
    li.setAttribute('role', 'option');
    li.setAttribute('tabindex', '-1');
    li.setAttribute('id', `${this.listboxId}-s${index}`);
    li.setAttribute('data-autocomplete-item', index);
    li.setAttribute('aria-posinset', index + 1);
    li.setAttribute('aria-selected', 'false');
    li.onblur = (e) => this.blurHandler(e);

    this.addClasses(li, this.options.classes.option);
    return li;
  }

  /**
   * Opens the suggestion list.
   *
   * @private
   */
  open() {
    this.input.setAttribute('aria-expanded', 'true');
    this.listboxWrapper.removeAttribute('hidden');
    this.listboxWrapper.style.zIndex = this.options.listZindex;
    this.isOpened = true;
    this.listboxWrapper.style.minWidth = `${this.input.offsetWidth - 4}px`;

    /**
     * Fires after the suggestion list opens.
     *
     * @event A11yAutocomplete#autocomplete-open
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     */
    this.triggerEvent('autocomplete-open');
    if (this.options.autoFocus) {
      this.preventCloseOnBlur = true;
      this.highlightItem(
        this.listboxWrapper.querySelector('[data-autocomplete-item="0"]'),
      );
    }
  }

  /**
   * Closes the suggestion list.
   *
   * @private
   */
  close() {
    if (this.isOpened) {
      this.input.setAttribute('aria-expanded', 'false');
      this.listboxWrapper.setAttribute('hidden', '');
      this.isOpened = false;

      /**
       * Fires after the suggestion list closes.
       *
       * @event A11yAutocomplete#autocomplete-close
       * @type {CustomEvent}
       * @prop {object} detail
       *  Detail property of the event.
       * @prop {A11yAutocomplete~Api} detail.autocomplete
       *  The autocomplete instance.
       */
      this.triggerEvent('autocomplete-close');
    }
  }

  /**
   * Returns the last value of an multi-value text field.
   *
   * @private
   * @return {string}
   *   The last value of the input field.
   */
  extractLastInputValue() {
    return this.splitValues().pop();
  }

  /**
   * Gets the input value.
   *
   * @private
   * @return {string}
   *   The input value.
   */
  inputValue() {
    return this.input.value;
  }

  /**
   * Helper splitting selections from the autocomplete value.
   *
   * @private
   * @return {Array}
   *   Array of values, split by comma.
   */
  splitValues() {
    const value = this.inputValue();
    const result = [];
    let quote = false;
    let current = '';
    const valueLength = value.length;
    for (let i = 0; i < valueLength; i++) {
      const character = value.charAt(i);
      if (character === '"') {
        current += character;
        quote = !quote;
      } else if (character === this.options.separatorChar && !quote) {
        result.push(current.trim());
        current = '';
      } else {
        current += character;
      }
    }
    if (value.length > 0) {
      result.push(current.trim());
    }
    return result;
  }

  /**
   * Determines if a suggestion should be an available option.
   *
   * @private
   * @param {object} suggestion
   *   A suggestion based on user input. It is an object with label and value
   *   properties.
   * @param {string} [typed='']
   *   The text entered in the input field.
   * @return {boolean}
   *   If the suggestion should be displayed in the results.
   */
  filterResults(suggestion, typed = '') {
    const suggestionLabel = suggestion.label || suggestion.value || suggestion;

    return RegExp(
      typed.trim().replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&'),
      'i',
    ).test(suggestionLabel);
  }

  /**
   * Prevent suggestions if the first input character is in the ignore list,
   * if the suggestion has already been added to the field.
   *
   * @private
   * @param {A11yAutocomplete~Suggestion[]} suggestions
   *  The list of suggestions to check for constraints.
   * @param {string[]} currentValues
   *  The current values inside the input field.
   * @return {A11yAutocomplete~Suggestion[]}
   *  The list of suggestions passing the different constraints.
   */
  applySelectionConstraints(suggestions, currentValues) {
    const { cardinality, allowRepeatValues } = this.options;

    if (cardinality > 0 && currentValues.length > cardinality) {
      return [];
    }

    if (allowRepeatValues) {
      return suggestions;
    }

    // Remove duplicates
    const filteredSuggestions = suggestions.filter(
      ({ value }) => !currentValues.includes(value),
    );
    // Reindex the suggestions in the set after filtering.
    filteredSuggestions.forEach(
      (suggestion, index) => (suggestion.index = index),
    );
    return filteredSuggestions;
  }

  /**
   * Sends a message to the configured live region.
   *
   * @private
   * @param {string} message
   *   The message to be sent to the live region.
   */
  sendToLiveRegion(message) {
    if (this.liveRegion) {
      this.liveRegion.textContent = message;
    }
  }

  /**
   * A message regarding the number of suggestions found.
   *
   * @private
   * @param {number} count
   *   The number of suggestions found.
   * @return {string}
   *   A message based on the number of suggestions found.
   */
  resultsMessage(count) {
    let message;
    if (count === 0) {
      message = this.options.noResultsAssistiveHint;
    } else if (count === 1) {
      message = this.options.oneResultAssistiveHint;
    } else {
      message = this.options.someResultsAssistiveHint;
    }

    return message.replace('@count', count);
  }

  /**
   * A message stating the number of characters needed to trigger autocomplete.
   *
   * @private
   * @return {string}
   *  The minimum characters message.
   */
  minCharsMessage() {
    if (this.options.minChars > 1) {
      return `${this.options.minCharAssistiveHint.replace(
        '@count',
        this.options.minChars,
      )}. `;
    }
    return '';
  }

  /**
   * Remove all event listeners added by this class.
   *
   * @callback A11yAutocomplete~destroy
   */
  destroy() {
    Object.keys(this.events).forEach((elementName) => {
      Object.keys(this.events[elementName]).forEach((eventName) => {
        this[elementName].removeEventListener(
          eventName,
          this.events[elementName][eventName],
        );
      });
    });
    if (this.listboxWrapper && this.listboxWrapper.parentNode) {
      this.listboxWrapper.parentNode.removeChild(this.listboxWrapper);
    }
    if (this.wrapper && this.wrapper.parentNode) {
      this.wrapper.parentNode.replaceChild(this.originalInput, this.wrapper);
    }

    /**
     * Fires after the instance is destroyed.
     *
     * @event A11yAutocomplete#autocomplete-destroy
     * @type {CustomEvent}
     * @prop {object} detail
     *  Detail property of the event.
     * @prop {A11yAutocomplete~Api} detail.autocomplete
     *  The autocomplete instance.
     */
    this.triggerEvent('autocomplete-destroy');
  }

  /**
   * Dispatches an autocomplete event
   *
   * @private
   * @param {string} type
   *   The event type.
   * @param {object} [additionalData={}]
   *   Additional data attached to the event's `details` property.
   * @param {boolean} [cancelable=false]
   *   If the dispatched event should be cancelable.
   * @param {Event} [originalEvent=null]
   *   A native event that called the function that triggers a custom event.
   * @return {boolean}
   *   If the event triggered successfully.
   */
  triggerEvent(
    type,
    additionalData = {},
    cancelable = false,
    originalEvent = null,
  ) {
    const event = new CustomEvent(type, {
      // we should probably set that to allow use of delegated events.
      bubbles: true,
      detail: {
        // Only exposed the "supported" set of methods/options.
        autocomplete: this.api,
        ...additionalData,
      },
      cancelable,
      originalEvent,
    });
    if (originalEvent) {
      event.originalEvent = originalEvent;
    }

    return this.input.dispatchEvent(event);
  }
}

/**
 * Main entrypoint to the library.
 *
 * @global
 * @param {HTMLElement} input
 *   The element to be used as an autocomplete.
 * @param {A11yAutocomplete~Options} options
 *  Autocomplete options.
 * @return {A11yAutocomplete~Api}
 *  API to manage an input's autocomplete functionality.
 * @fires A11yAutocomplete#autocomplete-change
 * @fires A11yAutocomplete#autocomplete-close
 * @fires A11yAutocomplete#autocomplete-created
 * @fires A11yAutocomplete#autocomplete-destroy
 * @fires A11yAutocomplete#autocomplete-highlight
 * @fires A11yAutocomplete#autocomplete-open
 * @fires A11yAutocomplete#autocomplete-pre-search
 * @fires A11yAutocomplete#autocomplete-response
 * @fires A11yAutocomplete#autocomplete-select
 * @fires A11yAutocomplete#autocomplete-selection-added
 */
const A11yAutocomplete = (input, options = {}) => {
  const autocomplete = new _A11yAutocomplete(input, options);
  return autocomplete.api;
};

export default A11yAutocomplete;
