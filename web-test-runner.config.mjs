export default {
  plugins: [
    {
      name: 'mouse-click',
      async executeCommand({ command, payload, session }) {
        if (command === 'mouse-click') {
          // handle specific behavior for playwright
          if (session.browser.type === 'playwright') {
            const page = session.browser.getPage(session.id);
            const { x, y, button = 'left' } = payload;
            await page.mouse.click(x, y, { button, delay: 1 });
            return true;
          }

          throw new Error(
            `Mouse interface is not supported for browser type ${session.browser.type}.`,
          );
        }
        return undefined;
      },
    },
  ],
  coverageConfig: {
    include: ['src/*'],
  },
};
